# Wind Energy Applications of OpenMDAO

OpenMDAO has been used extensively at DTU Wind in two main contexts: blade design and wind farm layout optimization.
Below, we provide a brief overview of these two application areas.

## HAWTOpt2: Aerostructural rotor design framework

HAWTOpt2 wraps three main codes analysis codes, HAWC2, HAWCStab2, and BECAS, to enable fully coupled aerostructural design of wind turbine blades, considering both the design of the internal structure of the blade, as well as the aerodynamic design.
Below, image shows the model hierarchy of the framework.
The primary use of OpenMDAO in this framework is to define the model hierarchy, parallelization of the execution, as well as the interfaces to various optimization libraries.
Since the analysis codes used do not provide analytic derivatives, gradients are computed using the finite difference method in this framework.

![](notebooks/images/hawtopt2_xdsm.png)

## Topfarm: Wind farm layout optimization tool

TopFarm wraps a number of models, primary focusing on stationary wake models provided by the PyWake open-source AEP calculator, that implements a variety of wake models, but also provides interfaces to more advanced flow models such as FUGA and EllipSys3D. TopFarm uses OpenMDAO as the underlying framework, but the user-facing interfaces do not expose OpenMDAO directly.
Many of the models in TopFarm also provide analytical derivatives, and as such TopFarm makes use of some of the mode advanced features of OpenMDAO not leveraged in HAWTOpt2.


![](notebooks/images/topfarm_schematic.png)


## WISDEM

[WISDEM](https://github.com/WISDEM/WISDEM) is a wind turbine optimization framework developed by NREL based on OpenMDAO, supporting aerostructural design of the rotor, tower and drivetrain design, as well as cost modelling. For specific optimizaiton scenarios, the user only interacts with the framework and OpenMDAO through input files offering an easier use of the framework. For advanced use cases, OpenMDAO workflows can be built by the user using the OpenMDAO groups and components defined in WISDEM.

![](notebooks/images/xdsm_wisdem.png)