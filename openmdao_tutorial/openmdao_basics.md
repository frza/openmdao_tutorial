
# OpenMDAO Basics

OpenMDAO uses object-oriented programming throughout its underlying framework, as well as for the user-facing interfaces that one interacts with when defining model hierarchies.
To define interfaces to your models as well as hierarchies of models you will need to do that within the confines of a set of pre-defined base classes that equips your model interface with the necessary methods and hooks needed by the underlying framework.
OpenMDAO operates with four main classes for defining a model hierarchy and how it should be executed, which is described below.

````{panels}

* Components implement the actual model computation
* Groups organize the model and enable hierarchical solver strategies
* Drivers iteratively execute the model (optimizers and DOEs)
* Problem is the top-level container


---

![](notebooks/images/om_hierarchy1.png)

````