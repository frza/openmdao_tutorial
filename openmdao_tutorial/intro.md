
# Introduction 

These pages contain a basic introduction to OpenMDAO, with a focus on hands-on examples based
on toy problems. These examples are heavily based on OpenMDAO's own documentation,
available here: https://openmdao.org/newdocs/versions/latest/main.html,
and we also borrowed material from https://github.com/OpenMDAO/openmdao_training.

This tutorial is interactive, and users can execute the individual notebooks using Binder.
Click on the icon below to start a Binder session:

[![Binder](https://mybinder.org/badge_logo.svg)](https://gesis.mybinder.org/v2/git/https%3A%2F%2Fgitlab.windenergy.dtu.dk%2Ffrza%2Fopenmdao_tutorial.git/HEAD?labpath=openmdao_tutorial)


## About OpenMDAO

From openmdao.org:

> OpenMDAO is an open-source high-performance computing platform for systems analysis and multidisciplinary optimization, written in Python.
> It enables you to decompose your models, making them easier to build and maintain, while still solving them in a tightly coupled manner with efficient parallel numerical methods.
> The OpenMDAO project is primarily focused on supporting gradient-based optimization with analytic derivatives to allow you to explore large design spaces with hundreds or thousands of design variables, but the framework also has a number of parallel computing features that can work with gradient-free optimization, mixed-integer nonlinear programming, and traditional design space exploration.

OpenMDAO is developed by a core team at NASA Glenn (USA) since 2008. For details of the theory behind OpenMDAO and to cite it, see {cite:p}`openmdao_2019,Hwang_maud_2018`

## Contents

```{tableofcontents}
```
