{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 1: Paraboloid - A Single-Discipline Model\n",
    "This tutorial focuses on using the [ExplicitComponent](https://openmdao.org/newdocs/versions/latest/features/core_features/working_with_components/explicit_component.html) class to build a simple analysis of a paraboloid function. We'll explain the basic structure of an OpenMDAO workflow, show you how to set inputs, run the model, and check the outputs. \n",
    "\n",
    "## The model\n",
    "\n",
    "Consider a paraboloid, defined by the explicit function\n",
    "\n",
    "$$\n",
    "  f(x,y) = (x-3.0)^2 + x \\times y + (y+4.0)^2 - 3.0 ,\n",
    "$$\n",
    "\n",
    "where `x` and `y` are the inputs to the function.\n",
    "The minimum of this function is located at\n",
    "\n",
    "$$\n",
    "  x = \\frac{20}{3} \\quad , \\quad y = -\\frac{22}{3} .\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ExplicitComponent\n",
    "\n",
    "Here is an OpenMDAO [ExplicitComponent](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/explicitcomponent.html) that defines this equation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import openmdao.api as om\n",
    "\n",
    "\n",
    "class Paraboloid(om.ExplicitComponent):\n",
    "    \"\"\"\n",
    "    Evaluates the equation f(x,y) = (x-3)^2 + xy + (y+4)^2 - 3.\n",
    "    \"\"\"\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input('x', val=0.0)\n",
    "        self.add_input('y', val=0.0)\n",
    "\n",
    "        self.add_output('f_xy', val=0.0)\n",
    "\n",
    "    def setup_partials(self):\n",
    "        # Finite difference all partials.\n",
    "        self.declare_partials('*', '*', method='fd')\n",
    "\n",
    "    def compute(self, inputs, outputs):\n",
    "        \"\"\"\n",
    "        f(x,y) = (x-3)^2 + xy + (y+4)^2 - 3\n",
    "\n",
    "        Minimum at: x = 6.6667; y = -7.3333\n",
    "        \"\"\"\n",
    "        x = inputs['x']\n",
    "        y = inputs['y']\n",
    "\n",
    "        outputs['f_xy'] = (x - 3.0)**2 + x * y + (y + 4.0)**2 - 3.0\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The component is the basic building block of a model. You will always define components as a subclass of either [ExplicitComponent](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/explicitcomponent.html) or [ImplicitComponent](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/implicitcomponent.html). Since our simple paraboloid function is explicit, we'll use the [ExplicitComponent](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/explicitcomponent.html). You see three methods defined:\n",
    "\n",
    "- `setup`: define all your inputs and outputs here\n",
    "- `setup_partials`: declare partial derivatives\n",
    "- `compute`: calculation of all output values for the given inputs\n",
    "\n",
    "In the `setup` method you define the inputs and outputs of the component.\n",
    "In the `setup_partials` method in this case you also ask OpenMDAO to approximate all the partial\n",
    "derivatives (derivatives of outputs with respect to inputs)\n",
    "using the [finite difference method](https://en.wikipedia.org/wiki/Finite_difference_method>).\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The model hierarchy\n",
    "\n",
    "Next, we need to define the model hierarchy, where we'll introduce the [Group](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/group.html) and [Problem](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/problem.html) classes, and how to execute the model hierarchy.\n",
    "\n",
    "**Preamble**\n",
    "\n",
    "The `openmdao.api` module is a convenient way to access classes defined in the OpenMDAO framework. It contains the class `ExplicitComponent` that we need to define the Paraboloid model, which can be accessed via \"om.ExplicitComponent\" when we need it. As you progress to more complex models, you will learn about more classes available in `openmdao.api`, but for now we only need this one to define our paraboloid component.\n",
    "\n",
    "```python\n",
    "import openmdao.api as om\n",
    "```\n",
    "\n",
    "**Group**\n",
    "\n",
    "All OpenMDAO models are built up from a hierarchy of [Group](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/group.html) instances that organize the components.\n",
    "In this example, the hierarchy is very simple, consisting of a single root group that holds a single component that is an\n",
    "instance of the `Paraboloid` class that we just defined.\n",
    "\n",
    "**Problem**\n",
    "\n",
    "Once the model hierarchy is defined,\n",
    "we pass it to the constructor of the [Problem](https://openmdao.org/newdocs/versions/latest/_srcdocs/packages/core/problem.html) class.\n",
    "Then we call the `setup()` method on that problem, which tells the framework to do some initial work to get the data structures in place for execution.\n",
    "In this case, we call `run_model()` to actually perform the computation. Later, we'll see how to explicitly set drivers and will be calling `run_driver()` instead.\n",
    "\n",
    "\n",
    "## Putting things together\n",
    "Here we called run_model twice.\n",
    "The first time `x` and `y` have the initial values of 3.0 and -4.0 respectively.\n",
    "The second time we changed those values to 5.0 and -2.0 and then re-ran.\n",
    "There are a few details to note here.\n",
    "First, notice the way we printed the outputs via `prob.get_val('parab_comp.f_xy')` and similarly how we set the new values for `x` and `y`.\n",
    "You can get and set values using the \"get_val\" and \"set_val\" methods on problem. By default, these methods will set and get values defined in\n",
    "the dimensional units of the specified input or output.\n",
    "In this case, there are no units on the source (i.e. `des_vars.x`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = om.Group()\n",
    "model.add_subsystem('parab_comp', Paraboloid())\n",
    "prob = om.Problem(model)\n",
    "prob.setup()\n",
    "\n",
    "prob.set_val('parab_comp.x', 3.0)\n",
    "prob.set_val('parab_comp.y', -4.0)\n",
    "\n",
    "prob.run_model()\n",
    "print(prob['parab_comp.f_xy'])\n",
    "\n",
    "prob.set_val('parab_comp.x', 5.0)\n",
    "prob.set_val('parab_comp.y', -2.0)\n",
    "\n",
    "prob.run_model()\n",
    "print(prob.get_val('parab_comp.f_xy'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualising the model hierarchy\n",
    "\n",
    "OpenMDAO has a number of nice interactive utilities, one of them is a visualisation tool for showing the model hierarchy, showing a so-called N2 diagram. What you notice in this diagram is an object named ´auto-IVC´. This object contains all independent variables, that is, variables that are externally set, such as design variables or other model inputs. If you wish to group independent variables, e.g. according to discipline, you can also add an instance of [IndepVarComp](https://openmdao.org/newdocs/versions/latest/features/core_features/working_with_components/indepvarcomp.html) to your model, and declare the independent variables, in our case, `x` and `y`. We will address this in one of the following tutorials."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "om.n2(prob, show_browser=False, embeddable=True, outfile='tutorial1_n2.html')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
