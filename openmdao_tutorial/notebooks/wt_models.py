import numpy as np

# Aero dynamics


def aero(r, chord, twist_deg, U0, tsr, pitch_deg=0, Cl_des=1.0, CloCd_des=None, air_density=1.225, NB=3, tiploss=False):
    """Simple analytical aero model 

    Parameters
    ----------
    r : ndarray
        rotor span [m]
    chord : ndarray
        blade chord [m]
    twist_deg : ndarray
        blade twist [deg]
    U0 : float
        free stream wind speed
    tsr : float
        tip-speed-ratio
    pitch_deg : int, optional
        blade pitch [deg], by default 0
    Cl_des : float, optional
        design lift-coefficient [-], by default 1.0
    CloCd_des : float or None, optional
        design glide ratio [-], by default None
    air_density : float, optional
        air density [kg/m³], by default 1.225   
    NB : int, optional
        number of blades, by default 3

    Returns
    -------
    dict
        CP  : float
            aerodynamic power coefficient [-]
        CT  : float
            aerodynamic thrust coefficient [-]
        CMxBR  : float
            aerodynamic blade root flap moment coefficient per blade [-]
        power  : float
            aerodynamic power [W]
        thrust  : float
            aerodynamic thrust [N]
        MxBR  : float
            aerodynamic blade root flap moment per blade [Nm]
        f_x  : ndarray
            tangential distributed force per blade [N/m]
        f_y  : ndarray
            normal distributed force per blade [N/m]
        f_z  : ndarray
            span wise distributed force per blade [N/m] (set to zero)
    """
    # Unpacking input
    R = r[-1]  # Rotor radius
    rt = r/R  # normalized span
    rho = air_density

    # Get optimal twist
    twist_opt_rad = twist_opt_rad_fun(rt, tsr)

    # Compute Cl
    Cl = Cl_fun(twist_deg, pitch_deg, Cl_des, twist_opt_rad)

    # Compute Cd/Cl
    CdoCl = CdoCl_fun(Cl, Cl_des, CloCd_des)

    # Tip-loss
    if tiploss:
        F = (2/np.pi*np.arccos(np.exp(-NB/2*np.sqrt(1+tsr**2)*(1+1e-5-rt))))
    else:
        F = 1.0

    # Compute local thrust
    CLT = CLT_fun(rt, R, chord, tsr, Cl, NB)

    # Compute local power
    CLP = (CLT-27/64*CLT**3 - np.sqrt((CLT*tsr*rt*CdoCl)**2))*F

    # Adding tip-loss for CLT
    CLT *= F

    # Compute global values
    CT = 2*trapz(CLT*rt, rt)
    CP = 2*trapz(CLP*rt, rt)
    CMxBR = 3*trapz(CLT*rt**2, rt)/NB
    return dict(
        CP=CP,
        CT=CT,
        CMxBR=CMxBR,
        power=0.5*rho*U0**3*np.pi*R**2*CP,
        thrust=0.5*rho*U0**2*np.pi*R**2*CT,
        MxBR=0.5*rho*U0**2*np.pi*R**3*CMxBR,
        f_x=0.5*rho*2*np.pi*R*U0**2*CLP/(tsr*NB),
        f_y=0.5*rho*2*np.pi*r*U0**2*CLT/(NB),
        f_z=np.zeros_like(r),
    )

def Cl_fun(twist_deg, pitch_deg, Cl_des, twist_opt_rad):
    # Cl=2*pi*sin(aoa-aoa_opt)+Cl_des
    return 2*np.pi*np.sin(deg2rad(twist_deg+pitch_deg)-twist_opt_rad) + Cl_des

def CdoCl_fun(Cl, Cl_des, CloCd_des):
    if not np.any(CloCd_des):
        return 0.0
    return 1/(np.exp(-(Cl-Cl_des)**2)*CloCd_des)

def CLT_fun(rt, R, chord, tsr, Cl, NB):
    return NB*tsr**2*rt/(2*np.pi*R)*chord*Cl


def twist_opt_rad_fun(rt, tsr):
    return np.arctan(2/(3*tsr*rt))


def chord_opt_fun(rt, R, tsr, NB, Cl_des):
    return 8/9*R*2*np.pi/(tsr**2*rt*NB*Cl_des)

# Beam


def beam(x, y, z, f_x, f_y, f_z, EIx, EIy):
    """Simple beam model

    Parameters
    ----------
    x : ndarray
        initial beam shape [m]
    y : ndarray
        initial beam shape [m]
    z : ndarray
        node location (span-wise) [m]
    f_x : ndarray
        distributed load [N/m]
    f_y : ndarray
        distributed load [N/m]
    f_z : ndarray
        distributed load [N/m] (not used)
    EIx : ndarray
        bending stiffness in the x direction
    EIy : ndarray
        bending stiffness in the y direction

    Returns
    -------
    dict
        contains
            d_x  : ndarray
                deflected shape in x (x0+dx) [m]
            d_y  : ndarray
                deflected shape in x (y0+dy) [m]
            d_z  : ndarray
                elongation z (z0+dz) [m] (not changed -> dz=0)
            t_x  : ndarray
                slope in x (d d_x/dx) [-]
            t_y  : ndarray
                slope in y (d d_y/dy) [-]
            t_z  : ndarray
                torsion angle (d d_z/dz) [-] (not changed)
    """
    # Normalizing z
    z0 = z-z[0]

    # Computing deflection in x
    d_x, t_x = compute_single_dir(z0, x, f_x, EIx)
    # Computing deflection in y
    d_y, t_y = compute_single_dir(z0, y, f_y, EIy)
    # Setting deflections to zero for z
    d_z = z.copy()
    t_z = np.zeros_like(d_z)

    return dict(
        d_x=d_x,
        d_y=d_y,
        d_z=d_z,
        t_x=t_x,
        t_y=t_y,
        t_z=t_z
    )

correct_exp = 2

def compute_single_dir(z0, x0, f_i, EI_i):
    # Compute deflection independently for each station
    L = z0[-1]
    correct_factor = ((L-z0)/L)**correct_exp
    t_i = t_fun(z0, L, f_i, EI_i)*correct_factor
    d_i = trapz_cumsum(t_i, z0)
    return d_i+x0, t_i


def t_fun(z0, L, f_i, EI_i):
    return f_i*z0*(3*L**2-3*L*z0+z0**2)/(6*EI_i)

# General


def deg2rad(angle):
    # Complex step safe
    return angle*np.pi/180.0


def rad2deg(angle):
    # Complex step safe
    return angle*180.0/np.pi


def trapz(y, x):
    # Complex step safe trapz method
    return np.sum(0.5*(y[1:]+y[:-1])*np.diff(x))


def trapz_cumsum(y, x):
    # Complex step safe
    out = np.zeros_like(y)
    out[1:] = np.cumsum((0.5*(y[1:]+y[:-1])*np.diff(x)))
    return out


if __name__ == "__main__":
    from scipy.optimize import brenth
    import matplotlib.pyplot as plt
    # Testing optimal power (Betz limit)
    rt = np.linspace(0, 1, 100)
    rt[0] += 1e-15
    R = 1
    r = rt*R
    tsr = 1
    NB = 1
    Cl_des = 1.0
    chord = chord_opt_fun(rt, R, tsr, NB, Cl_des)
    twist_deg = rad2deg(twist_opt_rad_fun(rt, tsr))

    out = aero(r, chord, twist_deg, 1.0, tsr, air_density=2.0/np.pi, NB=NB, CloCd_des=None)
    np.testing.assert_almost_equal(out["f_y"]/(2*rt), 8/9)
    np.testing.assert_almost_equal(out["thrust"], 8/9)
    np.testing.assert_almost_equal(out["f_x"]/2, 16/27)
    np.testing.assert_almost_equal(out["power"], 16/27)

    # Testing complex step
    # Optimizing tip chord
    h = 1e-40
    chord_tip_opt = chord[-1]
    chord[-1] /= 2
    out = aero(r, chord, twist_deg, 1.0, tsr, air_density=2.0/np.pi, NB=NB, CloCd_des=None)
    np.testing.assert_almost_equal((out["f_x"]/2)[:-1], 16/27)
    np.testing.assert_array_less((out["f_x"]/2)[-1], 16/27)
    np.testing.assert_array_less(out["power"], 16/27)

    def obj_fun(chord_tip):
        chord[-1] = chord_tip+1j*h
        out = aero(r, chord, twist_deg, 1.0, tsr, air_density=2.0/np.pi, NB=NB, CloCd_des=None)
        power = np.real(out["power"])
        dpower = np.imag(out["power"])/h
        return dpower
    chord = np.asarray(chord, dtype=complex)
    chord_tip = brenth(obj_fun, chord_tip_opt/4, chord_tip_opt*1.2)
    chord = np.real(chord)
    chord[-1] = chord_tip

    out = aero(r, chord, twist_deg, 1.0, tsr, air_density=2.0/np.pi, NB=NB, CloCd_des=None)
    np.testing.assert_almost_equal(out["f_y"]/(2*rt), 8/9)
    np.testing.assert_almost_equal(out["thrust"], 8/9)
    np.testing.assert_almost_equal(out["f_x"]/2, 16/27)
    np.testing.assert_almost_equal(out["power"], 16/27)

    # Optimizing tip twist
    twist_tip_opt = twist_deg[-1]
    twist_deg[-1] -= 2
    out = aero(r, chord, twist_deg, 1.0, tsr, air_density=2.0/np.pi, NB=NB, CloCd_des=None)
    np.testing.assert_almost_equal((out["f_x"]/2)[:-1], 16/27)
    np.testing.assert_array_less((out["f_x"]/2)[-1], 16/27)
    np.testing.assert_array_less(out["power"], 16/27)

    def obj_fun(twist_deg_tip):
        twist_deg[-1] = twist_deg_tip+1j*h
        out = aero(r, chord, twist_deg, 1.0, tsr, air_density=2.0/np.pi, NB=NB, CloCd_des=None)
        power = np.real(out["power"])
        dpower = np.imag(out["power"])/h
        return dpower
    twist_deg = np.asarray(twist_deg, dtype=complex)
    twist_tip = brenth(obj_fun, twist_tip_opt-4, twist_tip_opt+4)
    twist_deg = np.real(twist_deg)
    twist_deg[-1] = twist_tip

    out = aero(r, chord, twist_deg, 1.0, tsr, air_density=2.0/np.pi, NB=NB, CloCd_des=None)
    np.testing.assert_almost_equal(out["f_y"]/(2*rt), 8/9)
    np.testing.assert_almost_equal(out["thrust"], 8/9)
    np.testing.assert_almost_equal(out["f_x"]/2, 16/27)
    np.testing.assert_almost_equal(out["power"], 16/27)

    # Plotting aero output with Cl/Cd and tip-loss
    tsr = 4.0
    npt = 20
    R = 100
    r = np.linspace(4.0, R, npt)
    rt = r/R
    U0 = 8.0
    Cl_des=1.25
    NB=3
    chord = chord_opt_fun(rt, R, tsr, NB, Cl_des)
    chord = np.full(npt, 2)
    chord = np.array([4.09645950e-06, 2.94105174e+01, 3.00000000e+01, 2.98638933e+01,
       2.39716971e+01, 2.26986202e+01, 2.04535308e+01, 1.63446941e+01,
       1.32907276e+01, 1.14738269e+01, 1.10565332e+01, 1.06118023e+01,
       1.02176572e+01, 1.02448214e+01, 1.04645487e+01, 1.10969948e+01,
       1.24449714e+01, 1.57056437e+01, 2.61268809e+01, 1.22431088e+00])
    twist_deg = rad2deg(twist_opt_rad_fun(rt, tsr))
    twist_deg = np.full(npt, 10)#np.linspace(20., 0., npt)
    twist_deg = np.array([50.        , 50.        , 50.        , 41.42617175, 34.79218776,
       28.49096079, 24.23931714, 22.23796031, 20.91433486, 19.58606397,
       17.48961669, 15.9113529 , 14.6879014 , 13.44187625, 12.42731441,
       11.62358395, 10.98803693, 10.45870502, 10.1514528 ,  4.78664494])
    out = aero(r, chord, twist_deg, U0, tsr, NB=NB, Cl_des=Cl_des, CloCd_des=80, tiploss=True)
    plot = True
    if plot:
        fig, axs = plt.subplots(2, 1)

        axs[0].plot(r, out["f_x"])
        axs[0].set_ylabel("fx")

        axs[1].plot(r, out["f_y"]/r)
        axs[1].set_xlabel("r")
        axs[1].set_ylabel("fy/r")

        fig.suptitle(f"CP={out['CP']:1.2f}, CT={out['CT']:1.2f}, CMxBR={out['CMxBR']:1.2f}")
        fig.tight_layout()

    # Beam convergence test
    correct_exp = 0
    def beam_defl_const_load(z, EI, f): 
        # With constant loading density (f)
        R = z[-1]
        r = z
        return f*r**2*(6*R**2-4*R*r+r**2)/(24*EI)

    def beam_defl_lin_load(z, EIr, EI, f): 
        # With linear increase load and varying EI
        R = z[-1]
        r = z/R
        return 11*np.pi/(120)*f/EIr*R**5*(2/33*(EI-1)*r**6+r**5/11-5/11*(EI-1)*r**4+10/11*(2/3*EI-5/3)*r**3+20/11*r**2)

    R = 1
    EIr = 1
    EI_rel = 1
    ns = [10, 20, 40, 80, 160]
    plot = False
    if plot:
        fig, axs = plt.subplots(2, 2)
    for n in ns:
        rt =np.linspace(0, 1, n) 
        z = rt*R
        fz = np.zeros_like(z)

        # Constant loading    
        x = np.zeros_like(z)
        EIx = np.ones_like(z)
        fx = np.ones_like(z)

        # Linear increasing load and EI
        y = np.zeros_like(z)
        EIy = EIr/(1+(EI_rel-1)*rt)
        fy = rt.copy()

        out = beam(x, y, z, fx, fy, fz, EIx, EIy)

        if plot:
            axs[0, 0].plot(z, out["d_x"], label=n)    
            axs[0, 1].plot(z, out["d_x"]-beam_defl_const_load(z, 1.0, 1.0), label=n)
            axs[1, 0].plot(z, out["d_y"], label=n)    
            axs[1, 1].plot(z, out["d_y"]-beam_defl_lin_load(z, 1.0, EI_rel, 1.0), label=n)

    if plot:    
        axs[0, 0].plot(z, beam_defl_const_load(z, 1.0, 1.0), "k", label="true")
        axs[1, 0].plot(z, beam_defl_lin_load(z, 1.0, EI_rel, 1.0), "k", label="true")

    plt.show()


